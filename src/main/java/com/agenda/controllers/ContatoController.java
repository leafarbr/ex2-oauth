package com.agenda.controllers;

import com.agenda.dtos.ContatoDTO;
import com.agenda.models.Contato;
import com.agenda.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping
    public Contato cadastraContato(@RequestBody ContatoDTO contato, Principal principal)
    {
        Contato objContato = new Contato();
        objContato.setUser(principal.getName());
        objContato.setEmail(contato.getEmail());
        objContato.setName(contato.getName());

        return contatoService.CadastrarContato(objContato);
    }

    @GetMapping
    public Iterable<Contato> consultarContatoPorUsuario(Principal principal)
    {
        return contatoService.ConsultarContatosporId_user(principal.getName());
    }

}
