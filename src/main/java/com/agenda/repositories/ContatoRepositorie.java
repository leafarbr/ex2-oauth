package com.agenda.repositories;

import com.agenda.models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ContatoRepositorie extends CrudRepository<Contato, Integer> {
    Iterable<Contato> findByUser(String user);
}
