package com.agenda.services;

import com.agenda.models.Contato;
import com.agenda.repositories.ContatoRepositorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepositorie contatoRepositorie;

    public Contato CadastrarContato(Contato pcontato)
    {
        return contatoRepositorie.save(pcontato);
    }

    public Iterable<Contato> ConsultarContatosporId_user(String pidcliente)
    {
        return contatoRepositorie.findByUser(pidcliente);

    }

}
